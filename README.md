### Node.js Rest API With Restify, Mongoose, JWT

---

This code was developed by Traversy Media

- [Video part 1](https://www.youtube.com/watch?v=bqn-sx0v-l0)
- [Video part 2](https://www.youtube.com/watch?v=oyYOobBuczM)

#### Setup

- you need a MongoDb database
- in `config.js` you need to provide a username and password for mongodb. See `MONGODB_URI`

#### Run

- `npm update`
- `npm run` or `npm run dev`
